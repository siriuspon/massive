#define _CRT_SECURE_NO_WARNINGS // ������� ���������� ������ ���, ����� ���� ������
#include <iostream>
#include <ctime>


int main()
{
	setlocale(0, "Rus"); // ��� ������ �������� ������ � �������
	std::cout << "N = 8 // �� ���� ������� ���� N � ������� :(";
	const int N = 8; // �� ���� ������� ���, ����� N ��������� � ����������, ������� ����� ����� ��� (��������)
	int array[N][N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j] << ' ';
		}
		std::cout << '\n';
	}
	std::cout << '\n';

	int sum = 0; // ����� ��������� � ������
	time_t t;
	time(&t);
	int day = localtime(&t)->tm_mday; // ���� ������

	for (int x = 0; x < N; x++)
	{
		sum += array[day % N][x];
	}
	std::cout << "����� ��������� � ������ �������, ������ ������� ����� ������� ������� �������� ����� ��������� �� N = " << sum << '\n';

}